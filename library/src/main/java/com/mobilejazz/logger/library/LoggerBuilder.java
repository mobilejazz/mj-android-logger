package com.mobilejazz.logger.library;

public class LoggerBuilder {

  private Logger logger;

  private LoggerBuilder(Logger logger) {
    this.logger = logger;
  }

  public static Logger createLogger(Logger logger) {
    return new LoggerBuilder(logger).getLogger();
  }

  private Logger getLogger() {
    return logger;
  }
}
