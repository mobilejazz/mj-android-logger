package com.mobilejazz.logger.library;

/** Logging interface. */
public interface Logger {

  /** Log a verbose message with optional format args. */
  void v(String tag, String message, Object... args);

  /** Log a verbose exception and a message with optional format args. */
  void v(String tag, Throwable t, String message, Object... args);

  /** Log a debug message with optional format args. */
  void d(String tag, String message, Object... args);

  /** Log a debug exception and a message with optional format args. */
  void d(String tag, Throwable t, String message, Object... args);

  /** Log an info message with optional format args. */
  void i(String tag, String message, Object... args);

  /** Log an info exception and a message with optional format args. */
  void i(String tag, Throwable t, String message, Object... args);

  /** Log a warning message with optional format args. */
  void w(String tag, String message, Object... args);

  /** Log a warning exception and a message with optional format args. */
  void w(String tag, Throwable t, String message, Object... args);

  /** Log an error message with optional format args. */
  void e(String tag, String message, Object... args);

  /** Log an error exception and a message with optional format args. */
  void e(String tag, Throwable t, String message, Object... args);

  /** Method to send issue to external servers or local files. */
  void sendIssue(String tag, String message);

  /** Sets a device detail with boolean type. */
  void setDeviceBoolean(String key, boolean value);

  /** Sets a device detail with string type. */
  void setDeviceString(String key, String value);

  /** Sets a device detail with integer type. */
  void setDeviceInteger(String key, Integer value);

  /** Sets a device detail with double type. */
  void setDeviceFloat(String key, Float value);

  /** Remove a device detail. */
  void removeDeviceKey(String key);

  /** Get the device identifier generated. */
  String getDeviceIdentifier();
}