package com.mobilejazz.logger;

import com.mobilejazz.logger.library.Logger;
import timber.log.Timber;

public class TimberLogger implements Logger {

  @Override public void v(String tag, String message, Object... args) {
    Timber.v(message);
  }

  @Override public void v(String tag, Throwable t, String message, Object... args) {
    Timber.v(message);
  }

  @Override public void d(String tag, String message, Object... args) {
    Timber.d(message);
  }

  @Override public void d(String tag, Throwable t, String message, Object... args) {
    Timber.d(message);
  }

  @Override public void i(String tag, String message, Object... args) {
    Timber.i(message);
  }

  @Override public void i(String tag, Throwable t, String message, Object... args) {
    Timber.w(message);
  }

  @Override public void w(String tag, String message, Object... args) {
    Timber.w(message);
  }

  @Override public void w(String tag, Throwable t, String message, Object... args) {
    Timber.w(message);
  }

  @Override public void e(String tag, String message, Object... args) {
    Timber.e(message);
  }

  @Override public void e(String tag, Throwable t, String message, Object... args) {
    Timber.e(message);
  }

  @Override public void sendIssue(String tag, String message) {

  }

  @Override public void setDeviceBoolean(String key, boolean value) {

  }

  @Override public void setDeviceString(String key, String value) {

  }

  @Override public void setDeviceInteger(String key, Integer value) {

  }

  @Override public void setDeviceFloat(String key, Float value) {

  }

  @Override public void removeDeviceKey(String key) {

  }

  @Override public String getDeviceIdentifier() {
    return null;
  }
}
